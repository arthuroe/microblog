# Welcome to Wellfound's microblog assessment!

## Goal

![Alt text](home.png)

<BR><BR>

### How to run locally?

```
# create a virtual environment
python3 -m venv env

# activate the environment
source env/bin/activate

# upgrade pip and install requirements
pip install -U pip
pip install -r requirements.txt

# run the application
flask run
```
